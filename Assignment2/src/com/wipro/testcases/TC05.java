package com.wipro.testcases;

import org.openqa.selenium.chrome.ChromeDriver;

import java.io.*;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import org.openqa.selenium.TakesScreenshot;
import com.google.common.util.concurrent.CycleDetectingLockFactory.Policy;
import com.wipro.testbase.TestBase;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class TC05 extends TestBase{
	BufferedWriter prize;
	@BeforeClass
	
	public void start() throws IOException {
	prize = new BufferedWriter(new FileWriter("prize.txt", false));
		bb=new ChromeDriver();
        bb.get("https://demo.opencart.com");
	}
	@AfterClass
	public void stop() throws IOException {
		//bb.close();
		prize.close();
	}
	@Test
	public void test1() throws InterruptedException, BiffException, FileNotFoundException, IOException  {
		Properties properties=new Properties();
		properties.load(new FileInputStream("D:\\20111741\\mas assignment3\\Assignment2\\config\\config.properties"));
		String url = properties.getProperty("url");
		bb.get(url);
		System.out.println(url);
		//Thread.sleep(5000);
	}
	@Test
	public void test2() throws InterruptedException 
	{
		bb.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a")).click();
		bb.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[2]/a")).click();
	}
	@Test
    public void test3() throws BiffException, IOException, InterruptedException {
          File f=new File("D:\\20111741\\mas assignment3\\Assignment2\\testdata\\login.xls");
          Workbook b=Workbook.getWorkbook(f);
        Sheet s=b.getSheet(0);
        int col=s.getColumns();
        String []str=new String[col];
        for(int i=0;i<col;i++){
            Cell c=s.getCell(i, 0);
            str[i]=c.getContents();
        }
        bb.findElement(By.xpath("//*[@id=\"input-email\"]")).sendKeys(str[0]);
        bb.findElement(By.xpath("//*[@id=\"input-password\"]")).sendKeys(str[1]);
       
        }
		@Test
		public void test4() throws IOException {
			 bb.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/form/input")).click();
		        TakesScreenshot sh=((TakesScreenshot)bb);
				 File SrcFile=sh.getScreenshotAs(OutputType.FILE);
				 File log3=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\log3.png");
				 FileUtils.copyFile(SrcFile, log3);
			
		}
		@Test
		public void test5()
	{
		bb.findElement(By.linkText("Your Store")).click();
		
	}
		@Test
		public void test6()
		{
			bb.findElement(By.linkText("Brands")).click();
		}
		@Test
		public void test7() throws IOException
		{
			bb.findElement(By.linkText("Apple")).click();
			TakesScreenshot sh=((TakesScreenshot)bb);
			 File SrcFile=sh.getScreenshotAs(OutputType.FILE);
			 File apple=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\apple.png");
			 FileUtils.copyFile(SrcFile, apple);
			 
			 bb.findElement(By.linkText("Brand")).click();
			 bb.findElement(By.linkText("Canon")).click();
			 TakesScreenshot sh1=((TakesScreenshot)bb);
			 File SrcFile1=sh1.getScreenshotAs(OutputType.FILE);
			 File canon=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\canon.png");
			 FileUtils.copyFile(SrcFile1, canon);
			 
			 bb.findElement(By.linkText("Brand")).click();
			 bb.findElement(By.linkText("Hewlett-Packard")).click();
			 TakesScreenshot sh2=((TakesScreenshot)bb);
			 File SrcFile2=sh2.getScreenshotAs(OutputType.FILE);
			 File hew=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\hew.png");
			 FileUtils.copyFile(SrcFile2, hew);
			 
			 bb.findElement(By.linkText("Brand")).click();
			 bb.findElement(By.linkText("Palm")).click();
			 TakesScreenshot sh3=((TakesScreenshot)bb);
			 File SrcFile3=sh3.getScreenshotAs(OutputType.FILE);
			 File palm=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\palm.png");
			 FileUtils.copyFile(SrcFile3, palm);
			 
			 bb.findElement(By.linkText("Brand")).click();
			 bb.findElement(By.linkText("Sony")).click();
			 TakesScreenshot sh4=((TakesScreenshot)bb);
			 File SrcFile4=sh4.getScreenshotAs(OutputType.FILE);
			 File sony=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\sony.png");
			 FileUtils.copyFile(SrcFile4, sony);
		}
		@Test
		public void test8()
		{
			bb.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a/span[2]")).click();
			bb.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[1]/a")).click();	
		}
	@Test
		public void test9()
		{
			bb.findElement(By.xpath("//*[@id=\"column-right\"]/div/a[13]")).click();
		}
		
		
		
		
}