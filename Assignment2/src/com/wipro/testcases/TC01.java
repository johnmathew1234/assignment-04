package com.wipro.testcases;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.*;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import com.google.common.util.concurrent.CycleDetectingLockFactory.Policy;
import com.wipro.testbase.TestBase;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class TC01 extends TestBase{
	BufferedWriter Policy;
	@BeforeClass
	
	public void start() throws IOException {
	Policy = new BufferedWriter(new FileWriter("Policy.txt", false));
		bb=new ChromeDriver();
        bb.get("https://demo.opencart.com");
	}
	@AfterClass
	public void stop() throws IOException {
		bb.close();
		Policy.close();
	}
	@Test
	public void test1() throws InterruptedException, BiffException, FileNotFoundException, IOException  {
		Properties properties=new Properties();
		properties.load(new FileInputStream("D:\\20111741\\mas assignment3\\Assignment2\\config\\config.properties"));
		String url = properties.getProperty("url");
		bb.get(url);
		System.out.println(url);
		Thread.sleep(5000);
	}
	@Test
	public void test2() throws InterruptedException {
		bb.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a")).click();
	     bb.findElement(By.linkText("Register")).click();
	     Thread.sleep(4000);    
	}
	@Test
    public void test3() throws BiffException, IOException, InterruptedException {
          File f=new File("D:\\20111741\\mas assignment3\\Assignment2\\testdata\\data.xls");
          Workbook b=Workbook.getWorkbook(f);
        Sheet s=b.getSheet(0);
        int col=s.getColumns();
        String []str=new String[col];
        for(int i=0;i<col;i++){
            Cell c=s.getCell(i, 0);
            str[i]=c.getContents();
        }
       bb.findElement(By.name("firstname")).sendKeys(str[0]);
        bb.findElement(By.name("lastname")).sendKeys(str[1]);
        bb.findElement(By.name("email")).sendKeys(str[2]);
       bb.findElement(By.name("telephone")).sendKeys(str[3]);
        bb.findElement(By.name("password")).sendKeys(str[4]);
        bb.findElement(By.name("confirm")).sendKeys(str[5]);
        Thread.sleep(2000);
     }
	@Test
	public void test4() throws IOException, InterruptedException{
        bb.findElement(By.xpath("//*[@id=\"content\"]/form/div/div/input[1]")).click();
        Policy.write("Checkbox is checked"+"\n");
      
	}  
	@Test
	public void test5() throws IOException
	{
		 bb.findElement(By.xpath("//*[@id=\"content\"]/form/div/div/input[2]")).click();
		 TakesScreenshot sh=((TakesScreenshot)bb);
		 File SrcFile=sh.getScreenshotAs(OutputType.FILE);
		 File out=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\out.png");
		 FileUtils.copyFile(SrcFile, out);
				 
	        Policy.write("Congragulations.Your New account has been succesfully created");
	        
	}
	
	@Test
	public void test6()
	{
		bb.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a")).click();
		bb.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[1]/a")).click();
		
	}
	@Test
	public void test7()
	{
		bb.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a/span[2]")).click();
		bb.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[5]/a")).click();
//		*[@id="top-links"]/ul/li[2]/ul/li[5]/a
	}
	
}
