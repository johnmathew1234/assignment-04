package com.wipro.testcases;

import org.openqa.selenium.chrome.ChromeDriver;

import java.io.*;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.*;
import org.openqa.selenium.TakesScreenshot;
import com.google.common.util.concurrent.CycleDetectingLockFactory.Policy;
import com.wipro.testbase.TestBase;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class TC4 extends TestBase{
	BufferedWriter Count;
	@BeforeClass
	
	public void start() throws IOException {
	Count= new BufferedWriter(new FileWriter("src\\Count.txt", false));
		bb=new ChromeDriver();
        bb.get("https://demo.opencart.com");
	}
	@AfterClass
	public void stop() throws IOException {
		//bb.close();
		Count.close();
	}
	@Test
	public void test1() throws InterruptedException, BiffException, FileNotFoundException, IOException  {
		Properties properties=new Properties();
		properties.load(new FileInputStream("D:\\20111741\\mas assignment3\\Assignment2\\config\\config.properties"));
		String url = properties.getProperty("url");
		bb.get(url);
		System.out.println(url);
//		Thread.sleep(5000);
	}
	@Test
	public void test2() throws InterruptedException 
	{
		bb.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a")).click();
		bb.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[2]/a")).click();
	}
	@Test
    public void test3() throws BiffException, IOException, InterruptedException {
          File f=new File("D:\\20111741\\mas assignment3\\Assignment2\\testdata\\login.xls");
          Workbook b=Workbook.getWorkbook(f);
        Sheet s=b.getSheet(0);
        int col=s.getColumns();
        String []str=new String[col];
        for(int i=0;i<col;i++){
            Cell c=s.getCell(i, 0);
            str[i]=c.getContents();
        }
        bb.findElement(By.xpath("//*[@id=\"input-email\"]")).sendKeys(str[0]);
        bb.findElement(By.xpath("//*[@id=\"input-password\"]")).sendKeys(str[1]);
       
        }
		@Test
		public void test4() throws IOException {
			 bb.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/form/input")).click();
		        TakesScreenshot sh=((TakesScreenshot)bb);
				 File SrcFile=sh.getScreenshotAs(OutputType.FILE);
				 File log4=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\log4.png");
				 FileUtils.copyFile(SrcFile, log4);
			}
		@Test
		public void test5() throws IOException
		{
			
			WebElement nav = bb.findElement(By.className("navbar-nav"));
			List<WebElement> navs =  nav.findElements(By.tagName("a"));
			int cnt=0;
			for(WebElement ele : navs) {
				if(!ele.getText().isEmpty())
				{
					Count.write(ele.getText()+"\n");
					cnt++;
				}
			}
			Count.write("Count = "+cnt);
			System.out.println("Count = "+cnt);
			}
		@Test
		public void test6() throws IOException
		{
			bb.findElement(By.xpath("//*[@id=\"menu\"]/div[2]/ul/li[1]/a ")).click();
			 TakesScreenshot sh=((TakesScreenshot)bb);
			 File SrcFile=sh.getScreenshotAs(OutputType.FILE);
			 File link1=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\link1.png");
			 FileUtils.copyFile(SrcFile, link1);
			bb.findElement(By.xpath("/html/body/div[1]/nav/div[2]/ul/li[2]/a")).click();
			 TakesScreenshot sh1=((TakesScreenshot)bb);
			 File SrcFile1=sh1.getScreenshotAs(OutputType.FILE);
			 File link2=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\link2.png");
			 FileUtils.copyFile(SrcFile1, link2);
			 bb.findElement(By.xpath("//*[@id=\"menu\"]/div[2]/ul/li[3]/a")).click();
			 TakesScreenshot sh2=((TakesScreenshot)bb);
			 File SrcFile2=sh2.getScreenshotAs(OutputType.FILE);
			 File link3=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\link3.png");
			 FileUtils.copyFile(SrcFile2, link3);
			 bb.findElement(By.xpath("//*[@id=\"menu\"]/div[2]/ul/li[4]/a")).click();
			 TakesScreenshot sh3=((TakesScreenshot)bb);
			 File SrcFile3=sh3.getScreenshotAs(OutputType.FILE);
			 File link4=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\link4.png");
			 FileUtils.copyFile(SrcFile3, link4);
			 bb.findElement(By.xpath("//*[@id=\"menu\"]/div[2]/ul/li[5]/a")).click();
			 TakesScreenshot sh4=((TakesScreenshot)bb);
			 File SrcFile4=sh4.getScreenshotAs(OutputType.FILE);
			 File link5=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\link5.png");
			 FileUtils.copyFile(SrcFile4, link5);
			 bb.findElement(By.xpath("//*[@id=\"menu\"]/div[2]/ul/li[6]/a")).click();
			 TakesScreenshot sh5=((TakesScreenshot)bb);
			 File SrcFile5=sh5.getScreenshotAs(OutputType.FILE);
			 File link6=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\link6.png");
			 FileUtils.copyFile(SrcFile5, link6);
			 bb.findElement(By.xpath("//*[@id=\"menu\"]/div[2]/ul/li[7]/a")).click();
			 TakesScreenshot sh6=((TakesScreenshot)bb);
			 File SrcFile6=sh6.getScreenshotAs(OutputType.FILE);
			 File link7=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\link7.png");
			 FileUtils.copyFile(SrcFile6, link7);
			 bb.findElement(By.xpath("//*[@id=\"menu\"]/div[2]/ul/li[8]/a")).click();
			 TakesScreenshot sh7=((TakesScreenshot)bb);
			 File SrcFile7=sh7.getScreenshotAs(OutputType.FILE);
			 File link8=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\link8.png");
			 FileUtils.copyFile(SrcFile7, link8);
			 
		}
		@Test
		public void test7()
			 {
			bb.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a/span[2]")).click();
			bb.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[1]/a")).click();
			 }
		@Test
		public void test8()
		{
			bb.findElement(By.xpath("//*[@id=\"column-right\"]/div/a[13]")).click();
		}
		
}
