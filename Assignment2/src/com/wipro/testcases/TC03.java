package com.wipro.testcases;

import org.openqa.selenium.chrome.ChromeDriver;

import java.io.*;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import org.openqa.selenium.TakesScreenshot;
import com.google.common.util.concurrent.CycleDetectingLockFactory.Policy;
import com.wipro.testbase.TestBase;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class TC03 extends TestBase{
	BufferedWriter prize;
	@BeforeClass
	
	public void start() throws IOException {
	prize = new BufferedWriter(new FileWriter("prize.txt", false));
		bb=new ChromeDriver();
        bb.get("https://demo.opencart.com");
	}
	@AfterClass
	public void stop() throws IOException {
		//bb.close();
		prize.close();
	}
	@Test
	public void test1() throws InterruptedException, BiffException, FileNotFoundException, IOException  {
		Properties properties=new Properties();
		properties.load(new FileInputStream("D:\\20111741\\mas assignment3\\Assignment2\\config\\config.properties"));
		String url = properties.getProperty("url");
		bb.get(url);
		System.out.println(url);
		Thread.sleep(5000);
	}
	@Test
	public void test2() throws InterruptedException 
	{
		bb.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a")).click();
		bb.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[2]/a")).click();
	}
	@Test
    public void test3() throws BiffException, IOException, InterruptedException {
          File f=new File("D:\\20111741\\mas assignment3\\Assignment2\\testdata\\login.xls");
          Workbook b=Workbook.getWorkbook(f);
        Sheet s=b.getSheet(0);
        int col=s.getColumns();
        String []str=new String[col];
        for(int i=0;i<col;i++){
            Cell c=s.getCell(i, 0);
            str[i]=c.getContents();
        }
        bb.findElement(By.xpath("//*[@id=\"input-email\"]")).sendKeys(str[0]);
        bb.findElement(By.xpath("//*[@id=\"input-password\"]")).sendKeys(str[1]);
       
        }
		@Test
		public void test4() throws IOException {
			 bb.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/form/input")).click();
		        TakesScreenshot sh=((TakesScreenshot)bb);
				 File SrcFile=sh.getScreenshotAs(OutputType.FILE);
				 File log3=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\log3.png");
				 FileUtils.copyFile(SrcFile, log3);
			
		}
		@Test
		public void test5() throws IOException
		{
			bb.findElement(By.xpath("//*[@id=\"menu\"]/div[2]/ul/li[3]/a")).click();
			bb.findElement(By.xpath("//*[@id=\"menu\"]/div[2]/ul/li[3]/div/div/ul/li[2]/a")).click();
			 TakesScreenshot sh=((TakesScreenshot)bb);
			 File SrcFile=sh.getScreenshotAs(OutputType.FILE);
			 File monitor=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\monitor.png");
			 FileUtils.copyFile(SrcFile, monitor);
		}
		@Test
		public void test6() throws IOException {
			String s=bb.findElement(By.xpath("//*[@id=\"content\"]/div[3]/div[1]/div/div[2]/div[1]/h4/a")).getText();
			String a=bb.findElement(By.xpath("//*[@id=\"content\"]/div[3]/div[1]/div/div[2]/div[1]/p[2]/span[1]")).getText();
			prize.write(s+"\n");
			prize.write(a);
		}
		@Test
		public void test7()
		{
			bb.findElement(By.xpath("//*[@id=\"content\"]/div[3]/div[1]/div/div[2]/div[2]/button[1]")).click();
		}
//		@Test
//		public void test8()
//		{
//			
//		}
		
		@Test
		public void test9()
		{
			
		}
		
		
		
}