package com.wipro.testcases;

import org.openqa.selenium.chrome.ChromeDriver;

import java.io.*;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import org.openqa.selenium.TakesScreenshot;
import com.google.common.util.concurrent.CycleDetectingLockFactory.Policy;
import com.wipro.testbase.TestBase;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class TC02 extends TestBase{
	BufferedWriter login;
	@BeforeClass
	
	public void start() throws IOException {
	login = new BufferedWriter(new FileWriter("login.txt", false));
		bb=new ChromeDriver();
        bb.get("https://demo.opencart.com");
	}
	@AfterClass
	public void stop() throws IOException {
		//bb.close();
		login.close();
	}
	@Test
	public void test1() throws InterruptedException, BiffException, FileNotFoundException, IOException  {
		Properties properties=new Properties();
		properties.load(new FileInputStream("D:\\20111741\\mas assignment3\\Assignment2\\config\\config.properties"));
		String url = properties.getProperty("url");
		bb.get(url);
		System.out.println(url);
		Thread.sleep(5000);
	}
	@Test
	public void test2() throws InterruptedException 
	{
		bb.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a")).click();
		bb.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[2]/a")).click();
	}
	@Test
    public void test3() throws BiffException, IOException, InterruptedException {
          File f=new File("D:\\20111741\\mas assignment3\\Assignment2\\testdata\\login.xls");
          Workbook b=Workbook.getWorkbook(f);
        Sheet s=b.getSheet(0);
        int col=s.getColumns();
        String []str=new String[col];
        for(int i=0;i<col;i++){
            Cell c=s.getCell(i, 0);
            str[i]=c.getContents();
        }
        bb.findElement(By.xpath("//*[@id=\"input-email\"]")).sendKeys(str[0]);
        bb.findElement(By.xpath("//*[@id=\"input-password\"]")).sendKeys(str[1]);
       
        }
		@Test
		public void test4() throws IOException {
			 bb.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/form/input")).click();
		        TakesScreenshot sh=((TakesScreenshot)bb);
				 File SrcFile=sh.getScreenshotAs(OutputType.FILE);
				 File log=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\log.png");
				 FileUtils.copyFile(SrcFile, log);
			
		}
		@Test
		public void test5() {
			bb.findElement(By.xpath("//*[@id=\"column-right\"]/div/a[2]")).click();
		}
		@Test
		public void test6()
		{	bb.findElement(By.xpath("//*[@id=\"input-telephone\"]")).clear();
			bb.findElement(By.xpath("//*[@id=\"input-telephone\"]")).sendKeys("7564");
		}
		
		@Test
		public void test7() throws IOException
		{
			bb.findElement(By.xpath("//*[@id=\"content\"]/form/div/div[2]/input")).click();
			login.write("Success:Your account has successfully been updated");
			 TakesScreenshot sh=((TakesScreenshot)bb);
			 File SrcFile=sh.getScreenshotAs(OutputType.FILE);
			 File update=new File("D:\\20111741\\mas assignment3\\Assignment2\\screenshots\\update.png");
			 FileUtils.copyFile(SrcFile, update);
			
		}
		@Test
		public void test8()
		{
			bb.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a")).click();
			bb.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[1]/a")).click();
		}
		@Test
	public void test9()
		{
		bb.findElement(By.xpath("//*[@id=\"column-right\"]/div/a[13]")).click();
	}

}



